import { LitElement, html } from '@polymer/lit-element'

import './setter'
import './getter'

class ContainerElement extends LitElement {
  _render(props) {
    return html`
<h1>Hello World</h1>
<setter-element value="20" min="1" max="100"></setter-element>
<getter-element balloons="5"></getter-element>
`
  }

  _firstRendered() {
    this.addEventListener('value-changed', (e) => { this.shadowRoot.querySelector('getter-element').balloons = e.detail})
  }
}

customElements.define('container-element', ContainerElement)
