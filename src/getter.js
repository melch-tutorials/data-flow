import { LitElement, html } from '@polymer/lit-element'

class GetterElement extends LitElement {

  static get properties() {
    return {
      balloons: Number
    }
  }

  _render(props) {
    return html`
<style>
    .balloonclass {
        color: red;
        font-size: 3em;
    }
</style>

<h2>Hello from getter</h1>
<p>No. of balloonns: ${props.balloons}</p>
<span class="balloonclass">${'🎈 '.repeat(props.balloons)}</span>
`
  }
}

customElements.define('getter-element', GetterElement)
